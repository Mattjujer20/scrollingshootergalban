﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartScene();
        }
    }

    private void RestartScene()
    {
        // Reactiva al jugador si estaba desactivado
        if (!Controller_Player._Player.gameObject.activeSelf)
        {
            Controller_Player._Player.gameObject.SetActive(true);
        }

        // Carga la escena actual (recargando la misma escena)
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Restaura el tiempo de escala después de cargar la escena
        Time.timeScale = 1;
    }
}
