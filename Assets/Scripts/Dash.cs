using UnityEngine;

public class PlayerDash : MonoBehaviour
{
    public float dashDistance = 5f; 
    public float dashDuration = 0.2f;
    public float dashCooldown = 1f; 

    private bool canDash = true; 
    private Vector3 initialScale; 
    private float dashTimer = 0f; 

    void Start()
    {
        
        initialScale = transform.localScale;
    }

    void Update()
    {
        // Verifica si se presiona la tecla Alt izquierda y si el jugador puede hacer un dash
        if (Input.GetKeyDown(KeyCode.LeftAlt) && canDash)
        {
            // Inicia el dash
            StartDash();
        }

        // Si el jugador está en medio de un dash
        if (dashTimer > 0f)
        {
            // Calcula la dirección del dash basándose en la entrada del jugador
            Vector3 dashDirection = GetDashDirection();

            // Calcula la nueva posición intermedia durante el dash usando Lerp
            float dashProgress = 1f - (dashTimer / dashDuration); // Calcula el progreso del dash (de 0 a 1)
            Vector3 dashPosition = transform.position + dashDirection * dashDistance * dashProgress;

            // Aplica la nueva posición al jugador
            transform.position = dashPosition;

            // Actualiza el temporizador del dash
            dashTimer -= Time.deltaTime;

            // Si el dash ha terminado, resetea el temporizador y permite al jugador volver a hacer un dash
            if (dashTimer <= 0f)
            {
                dashTimer = 0f;
                canDash = true;

                // Restaura la escala del jugador
                transform.localScale = initialScale;
            }
        }
    }

    // Método para iniciar el dash
    void StartDash()
    {
        // Inicia el temporizador del dash
        dashTimer = dashDuration;

        // Guarda la escala actual del jugador
        initialScale = transform.localScale;

        // Evita que el jugador pueda hacer otro dash hasta que el cooldown haya terminado
        canDash = false;

        // Puedes agregar efectos visuales o sonidos aquí para indicar que el jugador está haciendo un dash
    }

    // Método para obtener la dirección del dash basándose en la entrada del jugador
    Vector3 GetDashDirection()
    {
        // Obtiene la dirección del input del jugador
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");

        // Si no hay entrada, devuelve la dirección hacia adelante (por defecto)
        if (horizontalInput == 0f && verticalInput == 0f)
        {
            return transform.forward;
        }

        // Crea un vector de dirección basado en la entrada del jugador
        Vector3 dashDirection = new Vector3(horizontalInput, 0f, verticalInput).normalized;

        return dashDirection;
    }
}