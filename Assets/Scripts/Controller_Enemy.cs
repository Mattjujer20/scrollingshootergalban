﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public float enemySpeed;

    public float xLimit;

    private float shootingCooldown;

    public GameObject enemyProjectile;

    public GameObject powerUp;

    /* Inicializa el tiempo de enfriamiento del disparo de los enemigos */
    void Start()
    {
        shootingCooldown = UnityEngine.Random.Range(1, 10);
    }

    /* Actualiza el tiempo del enfriamiento del disparo y verifica si los enemigos
     * alcanzaron el limie del eje X y llama a la siguiente funcion "ShootPlayer"*/
    public virtual void Update()
    {
        shootingCooldown -= Time.deltaTime;
        CheckLimits();
        ShootPlayer();
    }

    /* Verifica si el jugador esta y si el tiempo para poder disparar paso  */
    void ShootPlayer()
    {
        if (Controller_Player._Player != null)
        {
            if (shootingCooldown <= 0)
            {
                Instantiate(enemyProjectile, transform.position, Quaternion.identity);
                shootingCooldown = UnityEngine.Random.Range(1, 10);
            }
        }
    }

    /* Verifica si la posicion del enemigo supero el limite en X para asi destruir el gameobject del enemigo */
    private void CheckLimits()
    {
        if (this.transform.position.x < xLimit)
        {
            Destroy(this.gameObject);
        }
    }

    /* se verifica si la colison ocurrio con un proyectil o un laser (ambos identificados por sus tags). 
     * si es así  se genera un power up y se destruyen tanto el enemigo como el proyectil/laser. */
    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            GeneratePowerUp();
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            Controller_Hud.points++;
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            GeneratePowerUp();
            Destroy(this.gameObject);
            Controller_Hud.points++;
        }
    }

    /* Genera un powerup en una posicion aleatorai cerca del enemigo */
    private void GeneratePowerUp()
    {
        int rnd = UnityEngine.Random.Range(0, 3);
        if (rnd == 2)
        {
            Instantiate(powerUp, transform.position, Quaternion.identity);
        }
    }
}
